import Vue from 'vue';
import App from './App.vue';
import store from './store/store';
import TableComponent from 'vue-table-component';

Vue.use(TableComponent);
TableComponent.settings({
  tableClass: '',
  theadClass: '',
  tbodyClass: '',
  filterPlaceholder: 'Search submissons',
  filterNoResults: 'There are no matching rows',
});

new Vue({
  el: '#app',
  store,
  render: h => h(App),
});
