import { STORAGE_KEY_TOKEN, QUESTIONS, SUBMISSIONS, PROJECTS, STORAGE_KEY_PROJECT_ID, USER_KEY, PROJECT_KEY } from './constants';

export const getToken = () => {
  return localStorage.getItem(STORAGE_KEY_TOKEN);
};

export const setToken = token => {
  localStorage.setItem(STORAGE_KEY_TOKEN, token);
};

export const setQuestions = questions => {
  localStorage.setItem(QUESTIONS, JSON.stringify(questions));
};

export const getQuestions = () => {
  return localStorage.getItem(QUESTIONS)!='undefined' ? JSON.parse(localStorage.getItem(QUESTIONS)) : [];
};

export const setSubmissions = submissions => {
  localStorage.setItem(SUBMISSIONS, JSON.stringify(submissions));
};

export const getSubmissions = () => {
  return localStorage.getItem(SUBMISSIONS)!='undefined' ? JSON.parse(localStorage.getItem(SUBMISSIONS)) : [];
};

export const setProjects = projects => {
  localStorage.setItem(PROJECTS, JSON.stringify(projects));
};

export const getProjects = () => {
  return localStorage.getItem(PROJECTS)!='undefined' ? JSON.parse(localStorage.getItem(PROJECTS)) : [];
};

export const deleteToken = () => {
  localStorage.removeItem(STORAGE_KEY_TOKEN);
};

export const getProjectId = () => {
  return parseInt(localStorage.getItem(STORAGE_KEY_PROJECT_ID), 10) || 0;
};

export const setProjectId = projectId => {
  localStorage.setItem(STORAGE_KEY_PROJECT_ID, projectId);
};
