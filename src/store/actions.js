import axios from 'axios';
import { getToken, setQuestions, setSubmissions, setProjects } from './storage';

const glResource = axios.create({
  baseURL: 'https://api.jotform.com/',
  method: 'GET'
});

glResource.interceptors.request.use(
  config => {
    return config;
  },
  error => Promise.reject(error)
);

export default {
  updateSelectedProjectId({ commit }, id) {
    commit('setSelectedProjectId', id);
  },
  toggleLoading({ commit }) {
    commit('toggleLoading');
  },
  toggleToken({ commit }) {
    commit('toggleToken');
  },
  async fetchProjects({ commit }) {
    const PROJECTS_PATH = `user/forms?apikey=${getToken()}&orderby=id`;
    const res = await glResource.get(PROJECTS_PATH);
    setProjects(res.data.content);
    commit('setProjects', res.data.content);
  },
  async fetchUser({ commit }) {
    const USER_PATH = 'user';
    const res = await glResource.get(USER_PATH);

    commit('setUser', res.data);
  },
  async fetchSubmissions({ commit, state }) {
    const SUBMISSIONS_PATH = `form/${state.selectedProjectId}/submissions?apikey=${getToken()}`;;
    const { data } = await glResource.get(SUBMISSIONS_PATH);
    setSubmissions(data.content)
    commit('setSubmissions', data.content);
  },
  async fetchQuestions({ commit, state }) {
    const QUESTIONS_PATH = `form/${state.selectedProjectId}/questions?apikey=${getToken()}`;;
    const { data } = await glResource.get(QUESTIONS_PATH);
    setQuestions(data.content)
    commit('setQuestions', data.content);
  },
};
