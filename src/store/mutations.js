import { TYPES } from './constants';

export default {
  toggleLoading(state) {
    state.isLoading = !state.isLoading;
  },
  toggleToken(state) {
    state.hasToken = !state.hasToken;
  },
  setProjects(state, projects) {
    state.projects = projects;
  },
  setSelectedProjectId(state, id) {
    state.selectedProjectId = id;
  },
  setUser(state, user) {
    state.user = user;
  },
  setSubmissions(state, submissions) {
    state.submissions = submissions.map(submission => {
      return { ...submission };
    });
  },
  setQuestions(state, questions) {
    state.questions = Object.keys(questions).map(question => {
      return { ...questions[question] };
    });
  },
};
