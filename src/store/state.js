import { getProjectId, getQuestions, getSubmissions, getProjects } from './storage';

export default {
  isLoading: !getQuestions() && !getSubmissions() && !getQuestions(),
  hasToken: false,
  selectedProjectId: getProjectId(),
  user: {},
  projects: getProjects(),
  submissions: getSubmissions(),
  questions: getSubmissions(),
  mrsAssignedToMe: [],
  mrsCreatedByMe: [],
  pipelines: [],
};
